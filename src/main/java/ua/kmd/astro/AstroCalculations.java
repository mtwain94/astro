package ua.kmd.astro;

import com.geo.tools.obj.BLH;
import com.geo.tools.obj.XYZ;


public class AstroCalculations {

    public static double getRA(double a1,double a2,double a3){
        return a1+a2/60+a3/3600;
    }

    public static double getDeclination(double a1,double a2,double a3){
        return a1+a2/60+a3/3600;
    }

    //TODO:name
    public static double getD(int day,int month,int year,int dHours){
        return 367.0*year-Math.floor((7*(year+(Math.floor((month+9)/12))))/4)+Math.floor((275*month)/9)+day-730530+((double)dHours)/24;
    }

    //TODO: name
    public static double getW(double d){
        return 282.9404 + 0.0000470935*d;
    }

    public static double getMeanAnomaly(double d){
        double m1 = 356.047+0.9856002585*d;
        double m2 = m1 - Math.floor(m1/360)*360;
        return (m2<0)?m2+360:m2;
    }

    public static double getMeanLongitude(double w,double meanAnomaly){
        double m1 = w+meanAnomaly;
        double m2 = m1 - Math.floor(m1/360)*360;
        return (m2<0)?m2+360:m2;
    }

    //TODO:name
    public static double getGMST0(double meanLongitude){
        return meanLongitude/15+12;
    }

    //TODO:name
    public static double getSIDTIME(double gmst0,int dHours,double spotterLongitude){
        return gmst0+dHours+spotterLongitude/15;
    }

    //TODO:name
    public static double getHA(double sidtime,double RA){
        //TODO:names
        return sidtime-RA;
    }

    public static double getH(double HA){
        return HA*15 - Math.floor(HA*15/360)*360 +((HA<0)?360:0);
    }

    public static XYZ getCelestialCS(double h,double declination){

        double x = Math.cos(Math.toRadians(h))*Math.cos(Math.toRadians(declination));
        double y = Math.sin(Math.toRadians(h))*Math.cos(Math.toRadians(declination));
        double z = Math.sin(Math.toRadians(declination));

        return new XYZ(x,y,z);
    }

    public static XYZ getHorizontalCS(XYZ celestialCS,double spotterLongitude){

        double x = celestialCS.getX()*Math.cos(Math.toRadians(90-spotterLongitude)) - celestialCS.getZ() * Math.sin(Math.toRadians(90-spotterLongitude));
        double y = celestialCS.getY();
        double z = celestialCS.getX()*Math.sin(Math.toRadians(90-spotterLongitude)) + celestialCS.getZ() * Math.cos(Math.toRadians(90-spotterLongitude));

        return new XYZ(x,y,z);
    }

    public static double getAz(XYZ horizontalCS){
        return Math.toDegrees(Math.atan2(horizontalCS.getY(),horizontalCS.getX()))+180;//TODO: метод Atan2(Y,X)
    }

    public static double getAlt(XYZ horizontalCS){
        return Math.toDegrees(Math.asin(horizontalCS.getZ()));
    }

    //Додатково

    public static double getRHor(double r,double alt){
        return r- Math.sin(Math.toRadians(alt));
    }

    public static double getRadiusVisible(double rHor){
        return Math.toDegrees(Math.atan(1737.4/(6378.1*rHor)));
    }

    public static double getLeftAz(double az,double radiusVisible){
        return az-radiusVisible;
    }

    public static double getCenterAz(double az){
        return az;
    }

    public static double getRightAz(double az,double radiusVisible){
        return az + radiusVisible;
    }

    //TODO: табличні обчислення

    //дирекційний кут

    public static int getL0(double spotterLongitude){
        return (int)Math.floor((spotterLongitude+3)/6)*6-3;
    }

    public static double getConvMeredians(BLH spotterCoordinates){
        return (spotterCoordinates.getL()-getL0(spotterCoordinates.getL()))*Math.sin(Math.toRadians(spotterCoordinates.getB()));
    }

    public static double getDirAngle(double az, double convMeredians){
        return (az - convMeredians)/6;
    }




}

import com.geo.tools.obj.BLH;
import com.geo.tools.obj.XYZ;
import org.junit.Assert;
import org.junit.Test;
import ua.kmd.astro.AstroCalculations;


public class RadAzhTest {

    int day = 9;
    int month = 2;
    int year = 2016;
    int hours = 22;
    int minutes = 0;
    int seconds = 0;
    int timeZone = 2;

    int dHours = hours-timeZone;

    BLH spotterCoordinates = new BLH(49d,28d,0);

    @Test
    public void testRadAzh() throws Exception {

        double RA = AstroCalculations.getRA(22d, 30d, 30.17502);
        Assert.assertEquals(22.50838195,RA, 0.00001);
        System.out.println("RA: " + RA);

        double declination = AstroCalculations.getDeclination(-8d, -22d, -37.93616887);
        Assert.assertEquals(-8.377204491,declination, 0.00001);
        System.out.println("declination: " + declination);

        double d = AstroCalculations.getD(day, month, year, dHours);
        Assert.assertEquals(5884.8333, d, 0.3);
        System.out.println("D: " + d);

        double w  = AstroCalculations.getW(d);
        Assert.assertEquals(283.217537, w, 0.1);
        System.out.println("W: " + w);

        double meanAnomaly = AstroCalculations.getMeanAnomaly(d);
        Assert.assertEquals(36.140255,meanAnomaly,0.1);
        System.out.println("meanAnomaly: " + meanAnomaly);

        double meanLongitude = AstroCalculations.getMeanLongitude(w, meanAnomaly);
        Assert.assertEquals(319.357792,meanLongitude,0.1);
        System.out.println("meanLongitude: " + meanLongitude);

        double gmst0 = AstroCalculations.getGMST0(meanLongitude);
        Assert.assertEquals(33.290519,gmst0,0.1);
        System.out.println("GMST0: " + gmst0);

        double sidtime = AstroCalculations.getSIDTIME(gmst0, dHours, spotterCoordinates.getL());
        Assert.assertEquals(55.157186,sidtime,0.1);
        System.out.println("SIDTIME: " + sidtime);

        double HA = AstroCalculations.getHA(sidtime, RA);
        Assert.assertEquals(32.648804,HA,0.1);
        System.out.println("HA: " + HA);

        double h = AstroCalculations.getH(HA);
        Assert.assertEquals(129.7320, h, 0.1);
        System.out.println("h: " + h);

        XYZ celestialCS = AstroCalculations.getCelestialCS(h, declination);
        Assert.assertEquals(-0.632378, celestialCS.getX(), 0.00001);
        System.out.println("cel X: " + celestialCS.getX());
        Assert.assertEquals(0.760837, celestialCS.getY(), 0.00001);//0.760837
        System.out.println("cel Y: " + celestialCS.getY());
        Assert.assertEquals(-0.145689, celestialCS.getZ(), 0.00001);
        System.out.println("cel Z: " + celestialCS.getZ());


        XYZ horizontalCS = AstroCalculations.getHorizontalCS(celestialCS,spotterCoordinates.getB());
        Assert.assertEquals(-0.381681, horizontalCS.getX(), 0.00001);
        System.out.println("hor X: " + horizontalCS.getX());
        Assert.assertEquals(0.760837, horizontalCS.getY(), 0.00001);//0.760836
        System.out.println("hor Y: " + horizontalCS.getY());
        Assert.assertEquals(-0.524831, horizontalCS.getZ(), 0.00001);//0.524830
        System.out.println("hor Z: " + horizontalCS.getZ());


        double az = AstroCalculations.getAz(horizontalCS);
        Assert.assertEquals(296.641077, az, 0.0001);//296.641104
        System.out.println("az: " + az);

        double alt = AstroCalculations.getAlt(horizontalCS);
        Assert.assertEquals(-31.656845, alt, 0.0001);//-31.656862
        System.out.println("alt: " + alt);

        double r_hor = AstroCalculations.getRHor(57.3281, alt);
        Assert.assertEquals(57.852902, r_hor, 0.0001);//57.852930
        System.out.println("r_hor: " + r_hor);

        double radiusVisible = AstroCalculations.getRadiusVisible(r_hor);
        Assert.assertEquals(0.269776, radiusVisible, 0.0001);//0.269775
        System.out.println("radius visible: " + radiusVisible);

        double azLeft = AstroCalculations.getLeftAz(az, radiusVisible);
        Assert.assertEquals(296.371301, azLeft, 0.0001);//296.371328
        System.out.println("az left: " + azLeft);

        double azCenter = AstroCalculations.getCenterAz(az);
        Assert.assertEquals(296.641077, azCenter, 0.0001);//296.641104
        System.out.println("az center: " + azCenter);

        double azRight = AstroCalculations.getRightAz(az, radiusVisible);
        Assert.assertEquals(296.910852, azRight, 0.0001);//296.910879
        System.out.println("az right: " + azRight);

        double lo = AstroCalculations.getL0(spotterCoordinates.getL());
        Assert.assertEquals(27, lo, 0.0001);
        System.out.println("LO: " + lo);

        double convMeredian = AstroCalculations.getConvMeredians(spotterCoordinates);
        Assert.assertEquals(0.7547095802, convMeredian, 0.0001);
        System.out.println("convMeredian: " + convMeredian);

        double azimuth = AstroCalculations.getDirAngle(az,convMeredian);
        Assert.assertEquals(49.31, azimuth, 0.01);
        System.out.println("azimuth: " + azimuth);









    }
}